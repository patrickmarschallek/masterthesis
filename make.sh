#!/bin/bash
BIN=/usr/local/texlive/2014/bin/x86_64-darwin/
PDF="pdflatex -interaction=nonstopmode -shell-escape $*"
DVI=dvips
PS2PDF=ps2pdf
BIBTEX="bibtex8 --wolfgang"
GLOSSARY=makeglossaries
INDEX=makeindex
NAME=main

$BIN/$PDF $NAME
$BIN/$PDF $NAME
$BIN/$BIBTEX $NAME.aux
$BIN/$INDEX $NAME.glo -s $NAME.ist -t $NAME.glg -o $NAME.gls
$BIN/$INDEX -s $NAME.ist -o $NAME.acr $NAME.acn # there is an error makeindex not found
$BIN/$GLOSSARY $NAME
#$BIN/$DVI $NAME
#/usr/local/bin/$PS2PDF $NAME.ps

for file in $NAME.acn $NAME.aux $NAME.bbl $NAME.glo $NAME.idx $NAME.alg $NAME.gls $NAME.glg $NAME.blg $NAME.ind $NAME.ist $NAME.lof $NAME.log $NAME.lot $NAME.out $NAME.acr $NAME.run.xml $NAME.toc $NAME-blx.bib $NAME.xdy $NAME.glsdefs $NAME.ilg $NAME.lof $NAME.lot $NAME.ps $NAME.dvi $NAME.idx $NAME.lol $NAME.mw
do
  if [ -f $file ]; then
    mv $file tmp/
  fi
done
