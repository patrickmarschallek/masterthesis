#!/bin/bash
BIN=/usr/local/texlive/2014/bin/x86_64-darwin/
export PATH=$PATH:$BIN
PDF="latex -shell-escape" #pdftex
#PDF="pdflatex -interaction=nonstopmode -shell-escape $*" #pdftex
DVI=dvips
PS2PDF=ps2pdf
BIBTEX="bibtex8 --wolfgang"
GLOSSARY=makeglossaries
INDEX=makeindex
NAME=main
CONVERT="mogrify -quality 100 -format eps *.jpg"
BASEDIR=$(pwd)
IMG_DIR="resources/img"

#convert all jpg files to eps files
cd $IMG_DIR
$CONVERT
rm *.jpg
cd $BASEDIR

# compile latex to pdf
$BIN/$PDF $NAME
$BIN/$BIBTEX $NAME.aux
$BIN/$PDF $NAME
$BIN/$PDF $NAME
$BIN/$INDEX $NAME.glo -s $NAME.ist -t $NAME.glg -o $NAME.gls
$BIN/$INDEX -s $NAME.ist -o $NAME.acr $NAME.acn # there is an error makeindex not found
$BIN/$GLOSSARY $NAME
$BIN/$PDF $NAME
$BIN/$PDF $NAME
# $BIN/$PDF $NAME
$BIN/$DVI $NAME
/usr/local/bin/$PS2PDF $NAME.ps

for file in $NAME.acn $NAME.aux $NAME.bbl $NAME.glo $NAME.idx $NAME.alg $NAME.gls $NAME.glg $NAME.blg $NAME.ind $NAME.ist $NAME.lof $NAME.log $NAME.lot $NAME.out $NAME.acr $NAME.run.xml $NAME.toc $NAME-blx.bib $NAME.xdy $NAME.glsdefs $NAME.ilg $NAME.lof $NAME.lot $NAME.ps $NAME.dvi $NAME.idx $NAME.lol $NAME.mw $NAME.out.ps
do
  if [ -f $file ]; then
    mv $file tmp/
  fi
done
